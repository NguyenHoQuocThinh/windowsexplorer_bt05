//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512544_Explorer.rc
//
#define IDC_MYICON                      2
#define IDD_MY1512544_EXPLORER_DIALOG   102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY1512544_EXPLORER          107
#define IDI_SMALL                       108
#define IDC_MY1512544_EXPLORER          109
#define IDC_TREEVIEW                    110
#define IDC_LISTVIEW                    111
#define IDC_ICONVIEW                    127
#define IDR_MAINFRAME                   128
#define IDC_SMALLICON                   128
#define IDB_BITMAP1                     129
#define IDC_DETAIL                      129
#define ID_VIEW_ICONS                   130
#define IDI_ICON1                       130
#define IDC_STATUSBAR                   130
#define IDC_ADDRESS                     131
#define IDC_ADDRESS_EDIT                132
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           133
#endif
#endif

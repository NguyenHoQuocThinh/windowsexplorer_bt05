#include "stdafx.h"
#include "main.h"

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MyRegisterClass(hInstance);

	if (!InitInstance(hInstance, nCmdShow))
		return false;

	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0))
		if (!IsDialogMessage(g_hWnd, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}//if

		return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	wc.cbClsExtra = 0;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wc.hIconSm = LoadIcon(wc.hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = _T("Chương Trình Windows Explorer đơn giản");
	wc.lpszMenuName = MAKEINTRESOURCE(IDC_MY1512544_EXPLORER);
	wc.style = CS_HREDRAW | CS_VREDRAW;

	return RegisterClassEx(&wc);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	g_hInst = hInstance;
	g_hWnd = CreateWindowEx(0, _T("Chương Trình Windows Explorer đơn giản"), _T("Windows Explorer"), 
		WS_OVERLAPPEDWINDOW | WS_SIZEBOX | WS_MAXIMIZE,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
		NULL, NULL, hInstance, NULL);

	if (!g_hWnd)
		return false;

	ShowWindow(g_hWnd, nCmdShow);
	UpdateWindow(g_hWnd);

	return true;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}
/*****************************************************************************************/

void OnDestroy(HWND hwnd)
{
	PostQuitMessage(0);
}

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(icc);
	icc.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icc);

	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight - 6, lf.lfWidth - 8,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	HWND other;

	RECT main; 
	GetWindowRect(hwnd, &main);

	g_Drive = new CDrive;
	g_Drive->GetSystemDrives();

	g_TreeView = new CTreeView;
	g_TreeView->Create(hwnd, IDC_TREEVIEW, g_hInst, main.right/3, main.bottom);
	g_TreeView->LoadMyComputer(g_Drive);
	g_TreeView->GetFocus();
	
	g_ListView = new CListView;
	g_ListView->Create(hwnd, IDC_LISTVIEW, g_hInst, (main.right - main.left)*2/3 + 1, main.bottom, main.right/3);
	g_ListView->LoadMyComputer(g_Drive);

	other = CreateWindow(L"BUTTON", L"Icon View", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 0, 0, 100, 75, hwnd, (HMENU)IDC_ICONVIEW, g_hInst, NULL);
	SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
	other = CreateWindow(L"BUTTON", L"Small Icon", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 100, 0, 100, 75, hwnd, (HMENU)IDC_SMALLICON, g_hInst, NULL);
	SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
	other = CreateWindow(L"BUTTON", L"List View", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 200, 0, 100, 75, hwnd, (HMENU)IDC_LISTVIEW, g_hInst, NULL);
	SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
	other = CreateWindow(L"BUTTON", L"Detail", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 300, 0, 100, 75, hwnd, (HMENU)IDC_DETAIL, g_hInst, NULL);
	SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
	//Đã khởi tạo xong ứng dụng
	g_bStarted = TRUE;

	return TRUE;
}

void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		MessageBox(NULL, L"Chương trình Window Explorer", L"ABOUT", MB_OK);
		break;
	case IDM_EXIT:
		DestroyWindow(hwnd);
		break;
	case IDC_ICONVIEW:
		g_ListView->ChangeViewOption(LVS_ICON);
		break;
	case IDC_SMALLICON:
		g_ListView->ChangeViewOption(LVS_SMALLICON);
		break;
	case IDC_LISTVIEW:
		g_ListView->ChangeViewOption(LVS_LIST);
		break;
	case IDC_DETAIL:
		g_ListView->ChangeViewOption(LV_VIEW_DETAILS);
		break;
	}
}

void OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	g_TreeView->Size(cy);
	GetWindowRect(g_TreeView->GetHandle(), &g_TreeViewRect);
	g_ListView->Size();
}

LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm)
{
	int nCurSelIndex;
	LPNMTOOLBAR lpnmToolBar = (LPNMTOOLBAR)pnm;

	if (g_bStarted)
	{
		LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;

		switch (pnm->code)
		{
		case TVN_ITEMEXPANDING:
			g_TreeView->PreloadExpanding(lpnmTree->itemOld.hItem, lpnmTree->itemNew.hItem);
			break;
		case TVN_SELCHANGED:
			g_TreeView->Expand(g_TreeView->GetCurSel());
			g_ListView->DeleteAll();
			g_ListView->LoadChild(g_TreeView->GetCurPath(), g_Drive);
			break;
		case NM_CLICK:
			if (pnm->hwndFrom == g_ListView->GetHandle()) {
				nCurSelIndex = ListView_GetNextItem(g_ListView->GetHandle(), -1, LVNI_FOCUSED);
				if (nCurSelIndex != -1)
					g_ListView->DisplayInfoCurSel();
			}
			break;
		case NM_DBLCLK:
			if (pnm->hwndFrom == g_ListView->GetHandle())
				g_ListView->LoadCurSel();
			break;
		case NM_CUSTOMDRAW:
			if (pnm->hwndFrom == g_TreeView->GetHandle())
				DoSizeTreeView();
			break;
		}
	}
	return 0;
}

void DoSizeTreeView()
{
	RECT newTreeRC;
	GetClientRect(g_TreeView->GetHandle(), &newTreeRC);

	if (newTreeRC.right != g_TreeViewRect.right)
	{
		g_ListView->Size();
		g_TreeViewRect = newTreeRC;
	}
}
/*==============
ĐẠI HỌC KHOA HỌC TỰ NHIÊN
Nguyễn Hồ Quốc Thịnh
MSSV: 1512544
MÔN HỌC: LẬP TRÌNH WINDOWS (lớp 2)
==============*/
* Các chức năng đã hoàn thành:
	- Đọc được ổ đĩa để tạo listview và treeview
	- Tạo được TreeView, bắt được sự kiện expand, duyệt nội dung thư mục bằng FindFistFile và FindNextFile.
	- Tạo ra được ListView, hiển thị được nội dung của thư mục, có thể double click vào thư mục để truy cập
	- Thay đổi được 4 kiểu hiện thị nội dung (Icon, SmallIcon, List, Detail)
* Luồng sự kiện chính:
	- Người dùng khởi động chương trình để duyệt thư mục, click vào thư mục bên TreeView hoặc double click vào thư mục bên ListView để truy cập vào bên trong.
* Luồng sự kiện phụ:
	- Người dùng muốn thoát khỏi thư mục hiện tại, chuyển sang thư mục khác chỉ cần click vào thư mục muốn đến bên TreeView.
Link demo youtube:https://youtu.be/k0iUmaaPFSc

*NOTE: TÀI LIỆU THAM KHẢO: FILE DEMO WINDOWS EXPLORER CỦA GIÁO VIÊN TRẦN DUY QUANG